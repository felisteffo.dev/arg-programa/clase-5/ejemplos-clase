package ar.edu.utnfc.argprog;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import ar.edu.utnfc.argprog.config.Configuracion;
import com.mysql.cj.jdbc.MysqlDataSource;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Philip
 */
@SuppressWarnings("ALL")
public class PruebaSqLite {

    private static int mIdNodo;
    private static MysqlDataSource mDs;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Class.forName("org.sqlite.JDBC");
//            DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
        }
        catch (ClassNotFoundException ex) {
            Logger.getLogger(PruebaSqLite.class.getName()).log(Level.SEVERE, null, ex);
        }
//        catch (SQLException ex)
//        {
//            Logger.getLogger(Prueba.class.getName()).log(Level.SEVERE, null, ex);
//        }

        regularQuery();

        preparedInserts();

    }

    private static void regularQuery() {
        try {
            Connection wCnn = crearConexionDM();
            Statement wStm = wCnn.createStatement();
            System.err.println("Statement Class: " + wStm.getClass());
            wStm.execute("select * from film_list where category = 'Music'");
            ResultSet wRs = wStm.getResultSet();
            System.err.println("ResultSet class: " + wRs.getClass());

            //Revisión de la estructura de la tabla obtenida
            System.out.println("Estructura de la tabla...");
            ResultSetMetaData wRsMD = wRs.getMetaData();
            for (int i = 1; i <= wRsMD.getColumnCount(); i++) {
                System.out.print(wRsMD.getColumnName(i) + "(" + wRsMD.getColumnTypeName(i) + ")");
                if (i < wRsMD.getColumnCount()) {
                    System.out.print(" -> ");
                }
            }
            System.out.println("\n");

            System.out.println("Listado...");
            for (int i = 1; i <= wRsMD.getColumnCount(); i++) {
                System.out.print(wRsMD.getColumnName(i));
                if (i < wRsMD.getColumnCount()) {
                    System.out.print(" -> ");
                }
            }
            while (wRs.next()) {
                System.out.print(wRs.getString("FID") + " -> ");
                System.out.print(wRs.getString(2) + " -> ");
                System.out.print(wRs.getString(3) + " -> ");
                System.out.print(wRs.getString(4) + " -> ");
                System.out.print(wRs.getDouble("price"));
                System.out.print(wRs.getInt("length"));
                System.out.print(wRs.getString("actors"));
                System.out.println();

            }

            wCnn.close();
        }
        catch (SQLException ex) {
            Logger.getLogger(PruebaSqLite.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    private static void preparedInserts() {
        Connection wCnn = null;
        try {
            wCnn = crearConexionDM();
            wCnn.setAutoCommit(false);  // Es ¡cómo! hacer BeginTransaction
            PreparedStatement wStm = wCnn.prepareStatement("Insert Into category"
                    + "(category_id, name, last_update) "
                    + "Values(?, ?,?)");

            wStm.setInt(1, 17);
            wStm.setString(2, "Adventure");
            wStm.setDate(3, new Date((new java.util.Date()).getTime()));
            wStm.execute();

            //Segunda Inserción
            wStm.setInt(1, 18);
            wStm.setString(2, "Fantasy");
            wStm.setDate(3, new Date((new java.util.Date()).getTime()));
            wStm.execute();

            wCnn.commit();

        }
        catch (SQLException ex) {
            try {
                wCnn.rollback();
                Logger.getLogger(PruebaSqLite.class.getName()).log(Level.SEVERE, null, ex);
            }
            catch (SQLException ex1) {
                Logger.getLogger(PruebaSqLite.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        finally {
            try {
                wCnn.close();
            }
            catch (SQLException ex) {
                Logger.getLogger(PruebaSqLite.class.getName()).log(Level.SEVERE, null, ex);
            }
        }


    }

    private static Connection crearConexionDM() {
        Connection wCnn = null;
        String dataBaseFilePath = Configuracion.getInstancia().getValue("databaseFile");
        try {
            wCnn = DriverManager.getConnection("jdbc:sqlite:" + dataBaseFilePath);
            //Para ver el nombre real de la clase
            System.err.println("Con DriverManager: " + wCnn.getClass());
        }
        catch (SQLException ex) {
            Logger.getLogger(PruebaSqLite.class.getName()).log(Level.SEVERE, null, ex);
        }

        return wCnn;

    }

}