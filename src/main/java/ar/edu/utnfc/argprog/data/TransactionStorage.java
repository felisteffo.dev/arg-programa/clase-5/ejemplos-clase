/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.edu.utnfc.argprog.data;

import java.sql.Connection;
import java.util.Hashtable;

/**
 *
 * @author Philip
 */
public class TransactionStorage
{
    	private static Hashtable<Long, Connection> mTransactions;

		static
		{
			mTransactions = new Hashtable<Long, Connection>();

		}

		public static Connection getTransaction(long pKey)
		{
			Connection wRes = null;
			if (mTransactions.containsKey(pKey))
				wRes = mTransactions.get(pKey);

			return wRes;

		}

		public static void addTransaction(long pKey, Connection pConnection)
		{
			mTransactions.put(pKey, pConnection);

		}

		public static void removeTransaction(long pKey)
		{

			if (mTransactions.containsKey(pKey))
				mTransactions.remove(pKey);

		}
}
