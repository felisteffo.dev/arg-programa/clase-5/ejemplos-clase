/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.edu.utnfc.argprog.data;

import com.mysql.cj.jdbc.MysqlDataSource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Philip
 */
public class ConnectedClassBaseMySql {
    private static MysqlDataSource ds = null;

    private boolean mModoTrans;
    protected long mTransId;
    private Connection mConnection;

    public boolean ismModoTrans() {
        return mModoTrans;
    }

    public long getmTransId() {
        return mTransId;
    }

    public ConnectedClassBaseMySql() {
        mModoTrans = false;
        mTransId = 0l;

    }

    public ConnectedClassBaseMySql(long pTransId) {
        mModoTrans = true;
        mTransId = pTransId;

    }

    public long beginTransaction() {
        Connection wCnn = getConnection();
        return beginTransaction(wCnn);

    }

    public long beginTransaction(Connection wCnn) {
        try {
            mTransId = System.currentTimeMillis();
            wCnn.setAutoCommit(false);
            TransactionStorage.addTransaction(mTransId, wCnn);
            mModoTrans = true;
        }
        catch (SQLException ex) {
            Logger.getLogger(ConnectedClassBaseMySql.class.getName()).log(Level.SEVERE, null, ex);
        }
        return mTransId;

    }

    public void setTransaction(long pTransId) {
        mModoTrans = true;
        mTransId = pTransId;

    }

    public void commitTransaction(long pTransId) {
        Connection wTx = TransactionStorage.getTransaction(pTransId);
        if (wTx != null) {
            try {
                wTx.commit();
                wTx.close();
                TransactionStorage.removeTransaction(pTransId);
            }
            catch (Exception ex) {
                //TODO tratar la exception
            }
        }

    }

    public void rollbackTransaction(long pTransId) {
        Connection wTx = TransactionStorage.getTransaction(pTransId);
        if (wTx != null) {
            try {
                wTx.rollback();
                wTx.close();
                TransactionStorage.removeTransaction(pTransId);
            }
            catch (Exception ex) {
                //TODO tratar la exception
            }
        }

    }

    protected void closeConnection() {
        try {
            if (!mModoTrans && !mConnection.isClosed()) {
                mConnection.close();
            }
        }
        catch (SQLException ex) {
            Logger.getLogger(ConnectedClassBaseMySql.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    protected Connection getConnection() {
        Connection wConn;
        if (mModoTrans) {
            wConn = TransactionStorage.getTransaction(mTransId);
            mConnection = wConn;
        }
        else {
            if (mConnection == null) {
                //mConnection = crearConexionDM();
                mConnection = crearConexionDS();
            }
            else {
                try {
                    if (mConnection.isClosed()) {
                        mConnection = null;
                        mConnection = getConnection();
                    }
                }
                catch (SQLException e) {
                    Logger.getLogger(ConnectedClassBaseMySql.class.getName()).log(Level.SEVERE, null, e);
                }

            }


        }
        return mConnection;

    }

    private Connection crearConexionDM() {
        Connection wCnn = null;
        try {
            wCnn = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila", "root", "mysql@dmin");
            //Para ver el nombre real de la clase
            System.err.println("Con DriverManager: " + wCnn.getClass());
        }
        catch (SQLException ex) {
            Logger.getLogger(ConnectedClassBaseMySql.class.getName()).log(Level.SEVERE, null, ex);
        }

        return wCnn;

    }

    private Connection crearConexionDS() {
        Connection wCnn = null;
        try {
            if (ds == null) {
                ds = new MysqlDataSource();
                ds.setServerName("localhost");
                ds.setDatabaseName("sakila");
                ds.setUser("root");
                ds.setPassword("mysql@dmin");
                ds.setPort(3306);
            }

            wCnn = ds.getConnection();
            //Para ver la clase
            System.err.println("Con DataSource: " + wCnn.getClass());


        }
        catch (SQLException ex) {
            Logger.getLogger(ConnectedClassBaseMySql.class.getName()).log(Level.SEVERE, null, ex);
        }

        return wCnn;
    }
}
