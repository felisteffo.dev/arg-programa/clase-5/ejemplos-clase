package ar.edu.utnfc.argprog.data;

import java.sql.*;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CustomerActiveRecord extends ConnectedClassBaseMySql
{
    private int customer_id;
    // ... demás atributos

    public CustomerActiveRecord()
    {
        super();
    }

    public CustomerActiveRecord(int custId)
    {
        customer_id = custId;
    }

    public ArrayList<HashMap<String, Object>> getCustomersByNombre(String nombre)
    {
        ArrayList<HashMap<String, Object>> result = new ArrayList<>();
        HashMap<String, Object> fila;
        try (Connection wCnn = getConnection())
        {
            Statement wStm = wCnn.createStatement();
            // como para ver un poco el modelo
            System.err.println("Statement Class: " + wStm.getClass());
            wStm.execute("select customer_id, store_id, first_name, last_name, email, address_id, active, create_date, last_update\n"
                            + "\tfrom customer\n"
                            + "\twhere first_name = '" + nombre + "'");
            ResultSet wRs = wStm.getResultSet();
            System.err.println("ResultSet class: " + wRs.getClass());

            //Revisión de la estructura de la tabla obtenida
            ResultSetMetaData wRsMD = wRs.getMetaData();

            while (wRs.next())
            {
                fila = new HashMap();
                for (int i = 1; i <= wRsMD.getColumnCount(); i++)
                {
                    fila.put(wRsMD.getColumnName(i), wRs.getObject(i));
                    //fila.put(wRsMD.getColumnName(i), wRs.getObject(wRsMD.getColumnName(i)));

                }
                result.add(fila);

            }

        }
        catch (SQLException e)
        {
            Logger.getLogger(CustomerActiveRecord.class.getName()).log(Level.SEVERE, null, e);
        }

        return result;
    }


    public ArrayList<HashMap<String, Object>> getUpdatedCustomers(Date date)
    {
        ArrayList<HashMap<String, Object>> result = new ArrayList<>();
        HashMap<String, Object> fila;
        try (Connection wCnn = getConnection())
        {
            PreparedStatement wPrep = wCnn.prepareStatement(
                                "select customer_id, store_id, first_name, last_name, email, address_id, active, create_date, last_update\n"
                                          + "\tfrom customer\n"
                                          + "\twhere last_update > ?");
            System.out.println(date);
            wPrep.setObject(1, date.toInstant()
                                                  .atZone(ZoneId.of("America/Argentina/Cordoba"))
                                                  .toLocalDate());
            // como para ver un poco el modelo
            System.err.println("Statement Class: " + wPrep.getClass());
            ResultSet wRs = wPrep.executeQuery();
            System.err.println("ResultSet class: " + wRs.getClass());

            //Revisión de la estructura de la tabla obtenida
            ResultSetMetaData wRsMD = wRs.getMetaData();

            while (wRs.next())
            {
                fila = new HashMap();
                for (int i = 1; i <= wRsMD.getColumnCount(); i++)
                {
                    fila.put(wRsMD.getColumnName(i), wRs.getObject(i));
                    //fila.put(wRsMD.getColumnName(i), wRs.getObject(wRsMD.getColumnName(i)));

                }
                result.add(fila);

            }

        }
        catch (SQLException e)
        {
            Logger.getLogger(CustomerActiveRecord.class.getName()).log(Level.SEVERE, null, e);
        }

        return result;
    }

    public double getCurrentCustomerBalance()
    {
        double wRes = 0;
        try (Connection wCnn = getConnection())
        {
            CallableStatement wStm =
                    wCnn.prepareCall("{? = call get_customer_balance(?, ?)}");

            wStm.registerOutParameter(1, Types.DECIMAL);

            wStm.setInt(2, customer_id);
            wStm.setObject(3, (new Date()).toInstant()
                                                      .atZone(ZoneId.of("America/Argentina/Cordoba"))
                                                      .toLocalDate());

            wStm.execute();

            wRes = wStm.getDouble(1);

            wCnn.close();

        }
        catch (SQLException ex)
        {
            Logger.getLogger(CustomerActiveRecord.class.getName()).log(Level.SEVERE, null, ex);
        }

        return wRes;
    }

}
