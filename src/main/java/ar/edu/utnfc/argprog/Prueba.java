package ar.edu.utnfc.argprog;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import com.mysql.cj.jdbc.MysqlDataSource;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Philip
 */
public class Prueba {

    private static int mIdNodo;
    private static MysqlDataSource mDs;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
//            DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
        }
        catch (ClassNotFoundException ex) {
            Logger.getLogger(Prueba.class.getName()).log(Level.SEVERE, null, ex);
        }
//        catch (SQLException ex)
//        {
//            Logger.getLogger(Prueba.class.getName()).log(Level.SEVERE, null, ex);
//        }

        regularQuery();

//        preparedInserts();
//
//        int enStock = runProcedure();
//
//        System.out.println("Cantidad de 'Alone Trip' en Stock en almacen 1: " + enStock);


    }

    private static void regularQuery() {
        try (Connection wCnn = crearConexionDM()) {
            Statement wStm = wCnn.createStatement();
            System.err.println("Statement Class: " + wStm.getClass());
            wStm.execute("select * from sakila.film_list where category = 'Music'");
            ResultSet wRs = wStm.getResultSet();
            System.err.println("ResultSet class: " + wRs.getClass());

            //Revisión de la estructura de la tabla obtenida
            System.out.println("Estructura de la tabla...");
            ResultSetMetaData wRsMD = wRs.getMetaData();
            for (int i = 1; i <= wRsMD.getColumnCount(); i++) {
                System.out.print(wRsMD.getColumnName(i) + "(" + wRsMD.getColumnTypeName(i) + ")");
                if (i < wRsMD.getColumnCount()) {
                    System.out.print(" -> ");
                }
            }
            System.out.println("\n");

            System.out.println("Listado...");
            for (int i = 1; i <= wRsMD.getColumnCount(); i++) {
                System.out.print(wRsMD.getColumnName(i));
                if (i < wRsMD.getColumnCount()) {
                    System.out.print(" -> ");
                }
            }
            while (wRs.next()) {
                System.out.print(wRs.getString("FID") + " -> ");
                System.out.print(wRs.getString(2) + " -> ");
                System.out.print(wRs.getString(3) + " -> ");
                System.out.print(wRs.getString(4) + " -> ");
                System.out.print(wRs.getDouble("price"));
                System.out.print(wRs.getInt("length"));
                System.out.print(wRs.getString("actors"));
                System.out.println();

            }

//            wCnn.close();
        }
        catch (SQLException ex) {
            Logger.getLogger(Prueba.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    private static void preparedInserts() {
        Connection wCnn = null;
        try {
            wCnn = crearConexionDS();
            wCnn.setAutoCommit(false); //Es ¡cómo! hacer BeginTransaction
            PreparedStatement wStm = wCnn.prepareStatement("Insert Into category"
                    + "(name, last_update) "
                    + "Values(?,?)");

            wStm.setString(1, "Adventure");
            wStm.setDate(2, new Date((new java.util.Date()).getTime()));
            wStm.execute();

            //Segunda Inserción
            wStm.setString(1, "Fantasy");
            wStm.setDate(2, new Date((new java.util.Date()).getTime()));
            wStm.execute();

            wCnn.commit();

        }
        catch (SQLException ex) {
            try {
                wCnn.rollback();
                Logger.getLogger(Prueba.class.getName()).log(Level.SEVERE, null, ex);
            }
            catch (SQLException ex1) {
                Logger.getLogger(Prueba.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        finally {
            try {
                wCnn.close();
            }
            catch (SQLException ex) {
                Logger.getLogger(Prueba.class.getName()).log(Level.SEVERE, null, ex);
            }
        }


    }

    private static int runProcedure() {
        Connection wCnn = null;
        int wRes = 0;
        try {
            wCnn = crearConexionDS();
            CallableStatement wStm = wCnn.prepareCall("{call film_in_stock(?, ?, ?)}");

            wStm.setInt(1, 17);
            wStm.setInt(2, 1);
            wStm.registerOutParameter(3, Types.INTEGER);

            wStm.execute();

            wRes = wStm.getInt("p_film_count");

            wCnn.close();

        }
        catch (SQLException ex) {
            Logger.getLogger(Prueba.class.getName()).log(Level.SEVERE, null, ex);
        }

        return wRes;
    }

    private static Connection crearConexionDM() {
        Connection wCnn = null;
        try {
//            wCnn = DriverManager.getConnection("jdbc:mysql://localhost:3306/sakila", "philip", "mysql@dmin");
            wCnn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/sakila", "philip", "mysql@dmin");
            //Para ver el nombre real de la clase
            System.err.println("Con DriverManager: " + wCnn.getClass());
        }
        catch (SQLException ex) {
            Logger.getLogger(Prueba.class.getName()).log(Level.SEVERE, null, ex);
        }

        return wCnn;

    }

    private static Connection crearConexionDS() {
        Connection wCnn = null;
        try {
            if (mDs == null) {
                mDs = new MysqlDataSource();
                mDs.setServerName("localhost");
                mDs.setDatabaseName("sakila");
                mDs.setUser("philip");
                mDs.setPassword("mysql@dmin");
                mDs.setPort(3306);

            }


            wCnn = mDs.getConnection();
            //Para ver la clase
            System.err.println("Con DataSource: " + wCnn.getClass());


        }
        catch (SQLException ex) {
            Logger.getLogger(Prueba.class.getName()).log(Level.SEVERE, null, ex);
        }

        return wCnn;
    }
}