package ar.edu.utnfc.argprog;

import ar.edu.utnfc.argprog.data.CustomerActiveRecord;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.stream.Collectors;


/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {

        CustomerActiveRecord custBd = new CustomerActiveRecord();

        ArrayList<HashMap<String, Object>> customers = custBd.getCustomersByNombre("TERRY");

        System.out.println("Clientes con nombre: TERRY (" + customers.size() + "):");
        System.out.println(customers.stream()
                .map(m -> m.entrySet().stream()
                        .map(entry -> "[attribute:" + entry.getKey() + " = " + entry.getValue() + "]")
                        .collect(Collectors.joining("\n")))
                .collect(Collectors.joining("\n*****************************************************************************************\n")));


        customers = custBd.getUpdatedCustomers(Date.from(Instant.parse("2019-11-01T18:35:24.00Z")));
        System.out.println("Clientes modificados después del '1/11/2019' (" + customers.size() + "): ");
        System.out.println(customers.stream()
                .map(m -> m.entrySet().stream()
                        .map(entry -> "[attribute:" + entry.getKey() + " = " + entry.getValue() + "]")
                        .collect(Collectors.joining("\n")))
                .collect(Collectors.joining("\n*****************************************************************************************\n")));

        System.out.print("Saldo del cliente con id= 546: ");

        CustomerActiveRecord cust546 = new CustomerActiveRecord(546);
        System.out.println(cust546.getCurrentCustomerBalance());


    }


}
